﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaDamageTrigger : MonoBehaviour
{
    public LayerMask targetLayers;
    public int damage = 10;
    public bool damageOnStart = true;
    public bool freezesEnemies = false;

    // Start is called before the first frame update
    void Start()
    {
        if(damageOnStart)
            DealAreaDamage();
    }

    void DealAreaDamage()
    {
        List<Collider2D> targets = new List<Collider2D>();
        ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.SetLayerMask(targetLayers);
        GetComponent<Collider2D>().OverlapCollider(contactFilter, targets);
        
        foreach(var target in targets)
        {
            if (target == null)
                continue;

            var hp = target.GetComponentInParent<HP>();
            if (hp)
                hp.Damage(damage);

            if (freezesEnemies)
            {
                var enemy = target.GetComponent<Enemy>();
                if (enemy)
                {
                    enemy.Freeze();
                }
            }
        }
    }
}
