﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets._2D;

public class Score : MonoBehaviour
{
    public static Score instance;
    public TMPro.TextMeshProUGUI goldText;
    public TMPro.TextMeshProUGUI killsText;
    public TMPro.TextMeshProUGUI killsTextGameOver;

    int _gold = 0;
    public int gold
    {
        get
        {
            return _gold;
        }
        set
        {
            goldText.text = value.ToString();
            _gold = value;
        }
    }

    int _kills = 0;
    public int kills
    {
        get
        {
            return _kills;
        }
        set
        {
            killsText.text = value.ToString();
            killsTextGameOver.text = value.ToString();
            _kills = value;
        }
    }

    private void Awake()
    {
        instance = this;
        kills = 0;
        gold = 200;

        StartCoroutine(GoldGainRoutine());
    }

    IEnumerator GoldGainRoutine()
    {
        while(gameObject.activeInHierarchy)
        {
            yield return new WaitForSeconds(0.5f);
            if(PlatformerCharacter2D.player != null)
                gold += 10;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CanBuy(int cost)
    {
        return gold >= cost;
    }

    public bool TryBuy(int cost)
    {
        if (gold >= cost)
        {
            gold -= cost;
            return true;
        }
        return false;
    }
}
