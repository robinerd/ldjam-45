﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Juicing : MonoBehaviour
{
    public float pulsateAmount = 0.0f;
    public float pulsateSpeed = 1.0f;
    public float rotate = 0.0f;
    public bool fadeOut = false;
    public float fadeOutSpeed = 1.0f;

    float time;
    Vector3 originalScale;

    // Start is called before the first frame update
    void Start()
    {
        originalScale = transform.localScale;
        time = Random.Range(0.0f, 20.0f);
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if (rotate != 0)
            transform.Rotate(0, 0, rotate * Time.deltaTime);

        if(pulsateAmount > 0)
        {
            transform.localScale = originalScale * (1 + pulsateAmount/2 + pulsateAmount * Mathf.Sin(time * pulsateSpeed));
        }

        if(fadeOut)
        {
            var sprite = GetComponent<SpriteRenderer>();
            if (sprite)
            {
                var col = sprite.color;
                col.a -= fadeOutSpeed * Time.deltaTime;
                sprite.color = col;
            }
        }
    }
}
