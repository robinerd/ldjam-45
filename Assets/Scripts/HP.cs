﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HP : MonoBehaviour
{
    public int maxHP = 100;
    public UnityEvent OnDeath;
    public Transform hpBar;

    public GameObject deathEffectPrefab;

    int hp;

    // Start is called before the first frame update
    void Start()
    {
        hp = maxHP;
    }

    private void OnDestroy()
    {
        if (hpBar)
            hpBar.localScale = new Vector3(0, 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (hpBar)
            hpBar.localScale = new Vector3((float)hp / maxHP, 1, 1);
    }

    public void Damage(int damage)
    {
        if (hp < 0)
            return; // Already dead

        hp -= damage;
        if (hp <= 0)
        {
            OnDeath.Invoke();
            Destroy(gameObject);

            if (deathEffectPrefab)
            {
                var deathEffect = Instantiate(deathEffectPrefab, transform.position, Quaternion.identity);
                Destroy(deathEffect, 3);
            }
        }
    }

    public void SetMaxHP(int maxHP)
    {
        this.maxHP = maxHP;
        hp = maxHP;
    }
}
