﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public float cooldown = 2.0f;

    float lastShootTime = 0;

    public void TryShootTowardsPosition(Vector3 position)
    {
        TryShoot((position - transform.position).normalized);
    }

    public virtual void TryShoot(Vector2 direction)
    {
        if (Time.time - lastShootTime >= cooldown)
        {
            lastShootTime = Time.time;

            var parentEnemy = GetComponentInParent<Enemy>();
            if (parentEnemy && parentEnemy.isFrozen)
                return;

            Shoot(direction);
        }
    }

    protected virtual void Shoot(Vector2 direction)
    {
    }

}
