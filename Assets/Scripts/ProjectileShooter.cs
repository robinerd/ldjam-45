﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : Shooter
{
    public Rigidbody2D projectilePrefab;
    public float projectileSpeed = 12;

    protected override void Shoot(Vector2 direction)
    {
        Rigidbody2D projectile = Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
        projectile.velocity = direction.normalized * projectileSpeed;
        Destroy(projectile.gameObject, 5);
    }
}
