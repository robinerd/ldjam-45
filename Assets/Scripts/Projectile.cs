﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //public int damage = 10;
    public GameObject hitEffectPrefab;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 8) // ground
        {
            hit();
        }

        else if (isTargetedLayer(other.gameObject.layer)) // enemy
        {
            //other.GetComponent<HP>().Damage(damage);
            hit();
        }
    }

    void hit()
    {
        Destroy(gameObject);
        if (hitEffectPrefab)
        {
            var hitEffect = Instantiate(hitEffectPrefab, transform.position, hitEffectPrefab.transform.rotation);
            Destroy(hitEffect, 7.0f);
        }
    }

    bool isTargetedLayer(int layer)
    {
        var areadamage = hitEffectPrefab.GetComponent<AreaDamageTrigger>();
        if (areadamage)
            return (areadamage.targetLayers | (1 << layer)) == areadamage.targetLayers;
        return false;
    }
}
