﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class Spawner : MonoBehaviour
{
    public Enemy basicMonster;
    public Enemy eliteMonster;

    float cooldown = 10.0f;
    int extraHp = 0;
    static int eliteCountdown = 20;
    float eliteInterval = 18.0f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(Random.Range(1.0f, 2.0f));
        while (gameObject.activeInHierarchy && PlatformerCharacter2D.player != null)
        {
            Spawn();
            yield return new WaitForSeconds(Random.Range(0.7f, 1.0f) * cooldown);
            if(cooldown > 4.0f)
                cooldown *= 0.97f;
        }
    }

    void Spawn()
    {
        var enemyPrefab = basicMonster;
        if(eliteCountdown <= 0)
        {
            enemyPrefab = eliteMonster;
            eliteCountdown = Mathf.RoundToInt(eliteInterval);
        }
        if (eliteInterval > 3)
            eliteInterval -= 0.2f;

        eliteCountdown--;

        var enemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
        var hp = enemy.GetComponent<HP>();
        if (hp)
            hp.SetMaxHP(hp.maxHP + extraHp);

        extraHp += 10;
    }
}
