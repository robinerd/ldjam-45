﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class BuildingSite : MonoBehaviour
{
    public Tower[] towerPrefabs;
    public int[] towerCosts;
    public GameObject buildTooltip;
    public TMPro.TextMeshProUGUI costText0;
    public TMPro.TextMeshProUGUI costText1;

    static int[] extraTowerCost = new int[2];

    static BuildingSite towerShowingTooltip = null;

    Tower currentTower = null;

    void Awake()
    {
        buildTooltip.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlatformerCharacter2D.player)
            return;

        if (towerShowingTooltip != null && towerShowingTooltip != this)
            return;

        bool playerInRange = Vector2.Distance(PlatformerCharacter2D.player.position, transform.position) < 2;
        if(!playerInRange)
        {
            if (towerShowingTooltip == this)
            {
                towerShowingTooltip = null;
                buildTooltip.SetActive(false);
            }
            return;
        }

        if(towerShowingTooltip == null && currentTower == null)
        {
            buildTooltip.SetActive(true);
            towerShowingTooltip = this;
        }

        if(towerShowingTooltip == this)
        {
            costText0.text = (towerCosts[0] + extraTowerCost[0]).ToString();
            costText1.text = (towerCosts[1] + extraTowerCost[1]).ToString();
            costText0.color = Score.instance.CanBuy(towerCosts[0] + extraTowerCost[0]) ? Color.white : Color.red;
            costText1.color = Score.instance.CanBuy(towerCosts[1] + extraTowerCost[1]) ? Color.white : Color.red;
        }

        if(currentTower == null)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                BuildTower(0);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                BuildTower(1);
            }
        }
    }

    public void BuildTower(int towerIndex)
    {
        if (!PlatformerCharacter2D.player)
            return;

        if (currentTower == null)
        {
            bool success = Score.instance.TryBuy(towerCosts[towerIndex] + extraTowerCost[towerIndex]);
            if (!success)
                return;

            placeTower(towerIndex);

            extraTowerCost[towerIndex] += 50;

            if (towerShowingTooltip == this)
            {
                towerShowingTooltip = null;
                buildTooltip.SetActive(false);
            }
            Destroy(gameObject);
        }
    }

    void placeTower(int towerIndex)
    {
        var prefab = towerPrefabs[towerIndex];
        currentTower = Instantiate(prefab, transform.position, transform.rotation);
    }
}
