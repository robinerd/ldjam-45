﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaShooter : Shooter
{
    public AreaDamageTrigger areaDamagePrefab;

    protected override void Shoot(Vector2 direction)
    {
        AreaDamageTrigger damager = Instantiate(areaDamagePrefab, transform.position, areaDamagePrefab.transform.rotation);
        Destroy(damager.gameObject, 4);
    }
}
