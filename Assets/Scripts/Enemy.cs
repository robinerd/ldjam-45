﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static Transform player;
    public static List<Enemy> enemies;

    public float speed = 5;
    public float acceleration = 1;
    public float randomAcceleration = 2;
    public GameObject frozenEffect;

    Rigidbody2D body;
    Vector2 targetDirection;
    float frozenTime = 0;

    public bool isFrozen
    {
        get { return frozenTime > 0; }
    }

    public void Died()
    {
        if (player == null)
            return;
        Score.instance.gold += 10;
        Score.instance.kills++;
    }

    private void OnEnable()
    {
        if (enemies == null)
            enemies = new List<Enemy>();

        enemies.Add(this);
    }

    private void OnDisable()
    {
        enemies.Add(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (player == null)
        {
            var playerObj = GameObject.FindGameObjectWithTag("Player");
            if (playerObj)
                player = playerObj.transform;
        }

        // Prevent swarm of monsters to be in sync.
        speed *= Random.Range(0.7f, 1.4f);
        acceleration *= Random.Range(0.8f, 1.2f);
        transform.localScale *= Random.Range(0.8f, 1.0f);

        body = GetComponent<Rigidbody2D>();
        GetComponent<Seeker>().pathCallback += OnPathFound;

        StartCoroutine(PathFinderCoroutine());
    }

    private void OnPathFound(Path p)
    {
        if (!p.IsDone() || p.path == null || p.path.Count < 2)
            return;

        targetDirection = (Vector3)p.path[1].position - (Vector3)body.position;
        targetDirection.Normalize();
    }

    void FixedUpdate()
    {
        if(frozenTime > 0)
        {
            body.velocity *= 0.8f;
            return;
        }

        Vector2 targetVelocity = targetDirection * speed;
        Vector2 force = (targetVelocity - body.velocity) * acceleration * body.mass;
        body.AddForce(force, ForceMode2D.Force);

        if(Random.value < 0.1f)
            body.AddForce(Random.insideUnitCircle * randomAcceleration * body.mass, ForceMode2D.Impulse);
    }

    private void Update()
    {
        if(frozenTime > 0)
        {
            frozenTime -= Time.deltaTime;
            if(frozenTime < 0)
            {
                frozenEffect.SetActive(false);
            }
        }

        if (player == null)
            return;

        bool needsFlip = false;
        if (transform.localScale.x > 0 && player.position.x < transform.position.x)
            needsFlip = true;
        else if (transform.localScale.x < 0 && player.position.x > transform.position.x)
            needsFlip = true;

        if (needsFlip)
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    private IEnumerator PathFinderCoroutine()
    {
        while (gameObject.activeInHierarchy)
        {
            yield return new WaitForSeconds(Random.Range(0.2f, 0.8f));

            if(player)
                GetComponent<Seeker>().StartPath(transform.position, player.position);
        }
    }

    public void Freeze()
    {
        frozenTime = 1.5f;

        if(frozenEffect)
            frozenEffect.SetActive(true);
    }
}
