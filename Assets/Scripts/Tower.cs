﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class Tower : MonoBehaviour
{
    public bool aimAtPlayer = false;
    public float maxRange = 10;

    Shooter shooter;
    Transform target;

    // Start is called before the first frame update
    void Start()
    {
        shooter = GetComponentInChildren<Shooter>();

        if (aimAtPlayer)
            target = PlatformerCharacter2D.player.transform;
        else
            StartCoroutine(enemyTargetingCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
            target = closestEnemy();

        if (target && Vector2.Distance(shooter.transform.position, target.transform.position) < maxRange)
            shooter.TryShootTowardsPosition(target.transform.position);
    }

    Transform closestEnemy()
    {
        Enemy closest = null;
        float closestDistance = 100000;
        foreach(var enemy in Enemy.enemies)
        {
            if (enemy == null)
                continue;

            float distance = Vector2.Distance(enemy.transform.position, shooter.transform.position);
            if (distance < closestDistance)
            {
                closest = enemy;
                closestDistance = distance;
            }
        }

        if (closest)
            return closest.transform;
        return null;
    }

    IEnumerator enemyTargetingCoroutine()
    {
        while (gameObject.activeInHierarchy)
        {
            yield return new WaitForSeconds(Random.Range(0.7f, 1.1f));
            target = closestEnemy();
        }
    }
}
